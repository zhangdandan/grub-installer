# THIS FILE IS GENERATED AUTOMATICALLY FROM THE D-I PO MASTER FILES
# The master files can be found under packages/po/
#
# DO NOT MODIFY THIS FILE DIRECTLY: SUCH CHANGES WILL BE LOST
#
# Polish messages for debian-installer.
# Copyright (C) 2003 Software in the Public Interest, Inc.
# This file is distributed under the same license as debian-installer.
# Copyright (C) 2004-2010 Bartosz Feński <fenio@debian.org>
#
#
# Translations from iso-codes:
#     Translations taken from ICU SVN on 2007-09-09
#
#      Translations from KDE:
#      - Jacek Stolarczyk <jacek@mer.chemia.polsl.gliwice.pl>
#
#   Tobias Toedter <t.toedter@gmx.net>, 2007.
#   Jakub Bogusz <qboosh@pld-linux.org>, 2009-2011.
#      Alastair McKinstry <mckinstry@computer.org>, 2001.
#      Alastair McKinstry, <mckinstry@debian.org>, 2004.
#      Andrzej M. Krzysztofowicz <ankry@mif.pg.gda.pl>, 2007.
#      Cezary Jackiewicz <cjackiewicz@poczta.onet.pl>, 2000-2001.
#      Free Software Foundation, Inc., 2000-2010.
#      Free Software Foundation, Inc., 2004-2009.
#      GNOME PL Team <translators@gnome.pl>, 2001.
#      Jakub Bogusz <qboosh@pld-linux.org>, 2007-2011.
#      Tomasz Z. Napierala <zen@debian.linux.org.pl>, 2004, 2006.
# Marcin Owsiany <porridge@debian.org>, 2011.
# Michał Kułach <michal.kulach@gmail.com>, 2012, 2013, 2014, 2015, 2016, 2017, 2018.
# Bartosz Feński <fenio@debian.org>, 2020.
# Matthaiks <kitynska@gmail.com>, 2021, 2022.
# Łukasz Dulny <BartekChom@poczta.onet.pl>, 2017.
#
msgid ""
msgstr ""
"Project-Id-Version: debian-installer\n"
"Report-Msgid-Bugs-To: grub-installer@packages.debian.org\n"
"POT-Creation-Date: 2023-09-14 20:02+0000\n"
"PO-Revision-Date: 2023-09-16 17:22+0000\n"
"Last-Translator: Matthaiks <kitynska@gmail.com>\n"
"Language-Team: Polish <debian-l10n-polish@lists.debian.org>\n"
"Language: pl\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=3; plural=n==1 ? 0 : n%10>=2 && n%10<=4 && (n%100<10 "
"|| n%100>=20) ? 1 : 2;\n"

#. Type: boolean
#. Description
#. :sl1:
#. Type: boolean
#. Description
#. :sl1:
#: ../grub-installer.templates:1001 ../grub-installer.templates:2001
msgid "Install the GRUB boot loader to your primary drive?"
msgstr "Zainstalować program rozruchowy GRUB na głównym dysku twardym?"

#. Type: boolean
#. Description
#. :sl1:
#. Type: boolean
#. Description
#. Very similar to grub2/disable_os_prober
#. :sl1:
#: ../grub-installer.templates:1001 ../grub-installer.templates:34001
msgid ""
"The following other operating systems have been detected on this computer: "
"${OS_LIST}"
msgstr ""
"Następujące systemy operacyjne zostały wykryte w komputerze: ${OS_LIST}"

#. Type: boolean
#. Description
#. :sl1:
#: ../grub-installer.templates:1001
msgid ""
"If all of your operating systems are listed above, then it should be safe to "
"install the boot loader to your primary drive (UEFI partition/boot record). "
"When your computer boots, you will be able to choose to load one of these "
"operating systems or the newly installed Debian system."
msgstr ""
"Jeśli powyższa lista zawiera wszystkie Twoje systemy operacyjne, to "
"instalacja programu rozruchowego na pierwszym dysku twardym (partycja UEFI / "
"główny rekord rozruchowy) powinna być bezpieczna. Podczas uruchamiania "
"komputera będzie możliwość wyboru spośród tych systemów operacyjnych i nowo "
"zainstalowanego systemu Debian."

#. Type: boolean
#. Description
#. :sl1:
#: ../grub-installer.templates:2001
msgid ""
"It seems that this new installation is the only operating system on this "
"computer. If so, it should be safe to install the GRUB boot loader to your "
"primary drive (UEFI partition/boot record)."
msgstr ""
"Wygląda na to, że ta instalacja jest jedynym systemem operacyjnym w tym "
"komputerze. Jeśli tak jest to instalacja programu rozruchowego na pierwszym "
"dysku (partycja UEFI / główny rekord rozruchowy) powinna być bezpieczna."

#. Type: boolean
#. Description
#. :sl1:
#: ../grub-installer.templates:2001
msgid ""
"Warning: If your computer has another operating system that the installer "
"failed to detect, this will make that operating system temporarily "
"unbootable, though GRUB can be manually configured later to boot it."
msgstr ""
"Ostrzeżenie: Jeśli na komputerze jest inny system operacyjny, którego "
"instalator nie wykrył, ta operacja sprawi, że uruchomienie tego systemu może "
"być niemożliwe. Można jednakże w późniejszym czasie ręcznie skonfigurować "
"GRUB-a odpowiednio."

#. Type: boolean
#. Description
#. :sl3:
#: ../grub-installer.templates:3001
msgid "Install the GRUB boot loader to the multipath device?"
msgstr "Zainstalować program rozruchowy GRUB na urządzeniu multipath?"

#. Type: boolean
#. Description
#. :sl3:
#: ../grub-installer.templates:3001
msgid "Installation of GRUB on multipath is experimental."
msgstr "Instalacja GRUB-a na urządzeniu multipath jest eksperymentalna."

#. Type: boolean
#. Description
#. :sl3:
#: ../grub-installer.templates:3001
msgid ""
"GRUB is always installed to the master boot record (MBR) of the multipath "
"device. It is also assumed that the WWID of this device is selected as boot "
"device in the system's FibreChannel adapter BIOS."
msgstr ""
"GRUB jest zawsze instalowany w głównym rekordzie rozruchowym urządzenia "
"multipath. Zakłada się również, że WWID tego urządzenia jest ustawiony jako "
"pierwszy w kolejności uruchamiania w BIOS-ie adaptera FibreChannel."

#. Type: boolean
#. Description
#. :sl3:
#: ../grub-installer.templates:3001
msgid "The GRUB root device is: ${GRUBROOT}."
msgstr "Głównym urządzeniem GRUB-a jest: ${GRUBROOT}."

#. Type: error
#. Description
#. :sl3:
#. Type: error
#. Description
#. :sl2:
#: ../grub-installer.templates:4001 ../grub-installer.templates:13001
msgid "Unable to configure GRUB"
msgstr "Nie można skonfigurować GRUB-a"

#. Type: error
#. Description
#. :sl3:
#: ../grub-installer.templates:4001
msgid "An error occurred while setting up GRUB for the multipath device."
msgstr "Wystąpił błąd podczas ustawiania GRUB-a dla urządzenia multipath."

#. Type: error
#. Description
#. :sl3:
#: ../grub-installer.templates:4001
msgid "The GRUB installation has been aborted."
msgstr "Instalacja GRUB-a została przerwana."

#. Type: string
#. Description
#. :sl2:
#. Type: select
#. Description
#. :sl2:
#: ../grub-installer.templates:5001 ../grub-installer.templates:6002
msgid "Device for boot loader installation:"
msgstr "Urządzenie do instalacji programu rozruchowego:"

#. Type: string
#. Description
#. :sl2:
#. Type: select
#. Description
#. :sl2:
#: ../grub-installer.templates:5001 ../grub-installer.templates:6002
msgid ""
"You need to make the newly installed system bootable, by installing the GRUB "
"boot loader on a bootable device. The usual way to do this is to install "
"GRUB to your primary drive (UEFI partition/boot record). You may instead "
"install GRUB to a different drive (or partition), or to removable media."
msgstr ""
"Poprzez instalację programu rozruchowego GRUB, na rozruchowym urządzeniu, "
"musisz umożliwić samoczynne uruchamianie zainstalowanego systemu. "
"Najpopularniejszym sposobem, aby to osiągnąć, jest instalacja GRUB-a na "
"pierwszym dysku twardym (partycja UEFI / główny rekord rozruchowy). Możesz "
"jednakże użyć innego miejsca na dysku, innego dysku czy nawet zewnętrznego "
"nośnika, jeśli chcesz."

#. Type: string
#. Description
#. :sl2:
#: ../grub-installer.templates:5001
msgid ""
"The device notation should be specified as a device in /dev. Below are some "
"examples:\n"
" - \"/dev/sda\" will install GRUB to your primary drive (UEFI partition/"
"boot\n"
"   record);\n"
" - \"/dev/sdb\" will install GRUB to a secondary drive (which may for "
"instance\n"
"   be a thumbdrive);\n"
" - \"/dev/fd0\" will install GRUB to a floppy."
msgstr ""
"Urządzenie powinno zostać określone jako urządzenie w /dev. Poniżej kilka "
"przykładów:\n"
" - \"/dev/sda\" zainstaluje GRUB-a na pierwszym dysku (partycja UEFI / "
"główny \n"
"   rekord rozruchowy);\n"
" - \"/dev/sdb\" użyje drugiego dysku (to może być napęd USB);\n"
" - \"/dev/fd0\" zainstaluje GRUB-a na dyskietce."

#. Type: select
#. Choices
#: ../grub-installer.templates:6001
msgid "Enter device manually"
msgstr "Wprowadź urządzenie ręcznie"

#. Type: password
#. Description
#. :sl2:
#: ../grub-installer.templates:7001
msgid "GRUB password:"
msgstr "Hasło GRUB-a:"

#. Type: password
#. Description
#. :sl2:
#: ../grub-installer.templates:7001
msgid ""
"The GRUB boot loader offers many powerful interactive features, which could "
"be used to compromise your system if unauthorized users have access to the "
"machine when it is starting up. To defend against this, you may choose a "
"password which will be required before editing menu entries or entering the "
"GRUB command-line interface. By default, any user will still be able to "
"start any menu entry without entering the password."
msgstr ""
"Program rozruchowy GRUB oferuje wiele interaktywnych funkcji, które mogą "
"zostać wykorzystane do przejęcia kontroli nad komputerem w momencie gdy "
"użytkownik będzie miał do niego dostęp w momencie uruchamiania. By obronić "
"się przed tym, możliwe jest ustawienie hasła, które będzie wymagane przed "
"dokonaniem edycji elementów menu lub przejściem do linii poleceń GRUB-a. "
"Domyślnie każdy użytkownik nadal będzie miał możliwość uruchomienia systemu "
"bez wprowadzania hasła."

#. Type: password
#. Description
#. :sl2:
#: ../grub-installer.templates:7001
msgid "If you do not wish to set a GRUB password, leave this field blank."
msgstr "Jeśli nie chcesz ustalić hasła GRUB-a, zostaw puste miejsce."

#. Type: password
#. Description
#. :sl2:
#: ../grub-installer.templates:8001
msgid "Re-enter password to verify:"
msgstr "Potwierdź hasło:"

#. Type: password
#. Description
#. :sl2:
#: ../grub-installer.templates:8001
msgid ""
"Please enter the same GRUB password again to verify that you have typed it "
"correctly."
msgstr ""
"Proszę wpisać to samo hasło dla GRUB-a ponownie, aby upewnić się, że zostało "
"wpisane poprawnie."

#. Type: error
#. Description
#. :sl2:
#: ../grub-installer.templates:9001
msgid "Password input error"
msgstr "Błąd podczas wprowadzania"

#. Type: error
#. Description
#. :sl2:
#: ../grub-installer.templates:9001
msgid "The two passwords you entered were not the same. Please try again."
msgstr ""
"Hasła, które zostały wpisane nie są identyczne. Proszę spróbować jeszcze raz."

#. Type: error
#. Description
#. :sl2:
#: ../grub-installer.templates:11001
msgid "GRUB installation failed"
msgstr "Instalacja GRUB-a nie powiodła się"

#. Type: error
#. Description
#. :sl2:
#: ../grub-installer.templates:11001
msgid ""
"The '${GRUB}' package failed to install into /target/. Without the GRUB boot "
"loader, the installed system will not boot."
msgstr ""
"Instalacja pakietu '${GRUB}' w /target/ nie powiodła się. Bez programu "
"rozruchowego GRUB system nie uruchomi się."

#. Type: error
#. Description
#. :sl2:
#: ../grub-installer.templates:12001
msgid "Unable to install GRUB in ${BOOTDEV}"
msgstr "Nie można zainstalować GRUB-a w ${BOOTDEV}"

#. Type: error
#. Description
#. :sl2:
#: ../grub-installer.templates:12001
msgid "Executing 'grub-install ${BOOTDEV}' failed."
msgstr "Uruchomienie 'grub install ${BOOTDEV}' nie powiodło się."

#. Type: error
#. Description
#. :sl2:
#. Type: error
#. Description
#. :sl2:
#: ../grub-installer.templates:12001 ../grub-installer.templates:13001
msgid "This is a fatal error."
msgstr "Błąd krytyczny."

#. Type: error
#. Description
#. :sl2:
#: ../grub-installer.templates:13001
msgid "Executing 'update-grub' failed."
msgstr "Uruchomienie 'update-grub' nie powiodło się."

#. Type: boolean
#. Description
#. :sl4:
#: ../grub-installer.templates:15001
msgid "Install GRUB?"
msgstr "Zainstalować GRUB?"

#. Type: boolean
#. Description
#. :sl4:
#: ../grub-installer.templates:15001
msgid ""
"GRUB 2 is the next generation of GNU GRUB, the boot loader that is commonly "
"used on i386/amd64 PCs. It is now also available for ${ARCH}."
msgstr ""
"GRUB 2 to następna generacja GNU GRUB, programu rozruchowego popularnego na "
"komputerach i386/amd64. Jest teraz dostępny dla ${ARCH}."

#. Type: boolean
#. Description
#. :sl4:
#: ../grub-installer.templates:15001
msgid ""
"It has interesting new features but is still experimental software for this "
"architecture. If you choose to install it, you should be prepared for "
"breakage, and have an idea on how to recover your system if it becomes "
"unbootable. You're advised not to try this in production environments."
msgstr ""
"Ma interesujące nowe funkcje, ale na tej architekturze nadal jest "
"oprogramowaniem eksperymentalnym. Jeśli zdecydujesz się na jego instalację, "
"przygotuj się na błędy i zorientuj się jak odzyskać system jeśli rozruch "
"zawiedzie. Nie zaleca się robić tego w środowiskach produkcyjnych."

#. Type: text
#. Description
#. :sl1:
#: ../grub-installer.templates:16001
msgid "Installing GRUB boot loader"
msgstr "Instalowanie programu rozruchowego GRUB"

#. Type: text
#. Description
#. :sl1:
#: ../grub-installer.templates:17001
msgid "Looking for other operating systems..."
msgstr "Poszukiwanie innych systemów operacyjnych..."

#. Type: text
#. Description
#. :sl1:
#: ../grub-installer.templates:18001
msgid "Installing the '${GRUB}' package..."
msgstr "Instalowanie pakietu '${GRUB}'..."

#. Type: text
#. Description
#. :sl1:
#: ../grub-installer.templates:19001
msgid "Determining GRUB boot device..."
msgstr "Ustalanie urządzenia rozruchowego GRUB..."

#. Type: text
#. Description
#. :sl1:
#: ../grub-installer.templates:20001
msgid "Running \"grub-install ${BOOTDEV}\"..."
msgstr "Uruchamianie \"grub install ${BOOTDEV}\"..."

#. Type: text
#. Description
#. :sl1:
#: ../grub-installer.templates:21001
msgid "Running \"update-grub\"..."
msgstr "Uruchamianie \"update-grub\"..."

#. Type: text
#. Description
#. :sl1:
#: ../grub-installer.templates:22001
msgid "Updating /etc/kernel-img.conf..."
msgstr "Aktualizowanie /etc/kernel-img.conf..."

#. Type: text
#. Description
#. :sl1:
#: ../grub-installer.templates:23001
msgid "Checking whether to force usage of the removable media path"
msgstr ""
"Sprawdzanie czy trzeba wymusić korzystanie ze ścieżki nośników wymiennych"

#. Type: text
#. Description
#. :sl1:
#: ../grub-installer.templates:24001
msgid "Mounting filesystems"
msgstr "Montowanie systemów plików"

#. Type: text
#. Description
#. :sl1:
#: ../grub-installer.templates:25001
msgid "Configuring grub-efi for future usage of the removable media path"
msgstr "Konfigurowanie grub-efi do korzystania ze ścieżki nośników wymiennych"

#. Type: text
#. Description
#. Main menu item
#. Translators: keep below 55 columns
#. :sl1:
#: ../grub-installer.templates:26001
msgid "Install the GRUB boot loader"
msgstr "Instalacja programu rozruchowego GRUB"

#. Type: text
#. Description
#. Rescue menu item
#. :sl2:
#: ../grub-installer.templates:27001
msgid "Reinstall GRUB boot loader"
msgstr "Ponowna instalacja programu rozruchowego GRUB"

#. Type: error
#. Description
#. :sl4:
#: ../grub-installer.templates:30001
msgid "Failed to mount ${PATH}"
msgstr "Nie udało się zamontować ${PATH}"

#. Type: error
#. Description
#. :sl4:
#: ../grub-installer.templates:30001
msgid "Mounting the ${FSTYPE} file system on ${PATH} failed."
msgstr "Zamontowanie systemu plików ${FSTYPE} na ${PATH} nie powiodło się."

#. Type: error
#. Description
#. :sl4:
#: ../grub-installer.templates:30001
msgid "Check /var/log/syslog or see virtual console 4 for the details."
msgstr "Sprawdź /var/log/messages lub wirtualną konsolę nr 4 po szczegóły."

#. Type: error
#. Description
#. :sl4:
#: ../grub-installer.templates:30001
msgid "Warning: Your system may be unbootable!"
msgstr "Uwaga: Twój system może się nie uruchamiać!"

#. Type: text
#. Description
#. Rescue menu item
#. :sl2:
#: ../grub-installer.templates:31001
msgid "Force GRUB installation to the EFI removable media path"
msgstr "Wymuszenie instalacji GRUB w ścieżce nośników wymiennych EFI"

#. Type: boolean
#. Description
#. :sl1:
#: ../grub-installer.templates:32001
msgid "Force GRUB installation to the EFI removable media path?"
msgstr "Wymusić instalację GRUB w ścieżce nośników wymiennych EFI?"

#. Type: boolean
#. Description
#. :sl1:
#: ../grub-installer.templates:32001
msgid ""
"It seems that this computer is configured to boot via EFI, but maybe that "
"configuration will not work for booting from the hard drive. Some EFI "
"firmware implementations do not meet the EFI specification (i.e. they are "
"buggy!) and do not support proper configuration of boot options from system "
"hard drives."
msgstr ""
"Wygląda na to, że ten komputer skonfigurowano do korzystania z rozruchu EFI, "
"lecz konfiguracja ta może nie działać przy rozruchu z dysku twardego. "
"Niektóre implementacje oprogramowania układowego EFI nie spełniają "
"specyfikacji EFI (tzn. są błędne!) i nie obsługują właściwej konfiguracji "
"opcji rozruchu z systemowych dysków twardych."

#. Type: boolean
#. Description
#. :sl1:
#: ../grub-installer.templates:32001
msgid ""
"A workaround for this problem is to install an extra copy of the EFI version "
"of the GRUB boot loader to a fallback location, the \"removable media path"
"\". Almost all EFI systems, no matter how buggy, will boot GRUB that way."
msgstr ""
"Obejściem tego problemu jest instalacja dodatkowej kopii programu "
"rozruchowego GRUB w wersji EFI w lokalizacji zapasowej, \"ścieżce nośników "
"wymiennych\". Właściwie wszystkie systemy EFI, bez względu na swoje "
"niedopracowanie, uruchomią w ten sposób GRUB-a."

#. Type: boolean
#. Description
#. :sl1:
#: ../grub-installer.templates:32001
msgid ""
"Warning: If the installer failed to detect another operating system that is "
"present on your computer that also depends on this fallback, installing GRUB "
"there will make that operating system temporarily unbootable. GRUB can be "
"manually configured later to boot it if necessary."
msgstr ""
"Ostrzeżenie: Jeśli instalatorowi nie uda się wykryć innego systemu "
"operacyjnego dostępnego w tym komputerze korzystającego również z tej "
"lokalizacji zapasowej, zainstalowanie tutaj GRUB-a sprawi, że nie będzie "
"można go uruchomić. Można w późniejszym czasie odpowiednio skonfigurować "
"GRUB-a ręcznie, jeśli będzie to konieczne."

#. Type: boolean
#. Description
#. Same as grub2/update_nvram
#. :sl1:
#: ../grub-installer.templates:33001
msgid "Update NVRAM variables to automatically boot into Debian?"
msgstr "Zaktualizować zmienne NVRAM, aby automatycznie ładować Debiana?"

#. Type: boolean
#. Description
#. Same as grub2/update_nvram
#. :sl1:
#: ../grub-installer.templates:33001
msgid ""
"GRUB can configure your platform's NVRAM variables so that it boots into "
"Debian automatically when powered on. However, you may prefer to disable "
"this behavior and avoid changes to your boot configuration. For example, if "
"your NVRAM variables have been set up such that your system contacts a PXE "
"server on every boot, this would preserve that behavior."
msgstr ""
"GRUB może skonfigurować zmienne NVRAM platformy tak, aby automatycznie "
"ładowała Debiana po uruchomieniu. Można też wyłączyć to zachowanie i nie "
"zmieniać konfiguracji rozruchu. To pozwala na przykład pozostawić ustawienia "
"zmiennych NVRAM takie, że system kontaktuje się z serwerem PXE przy każdym "
"uruchomieniu."

#. Type: boolean
#. Description
#. Very similar to grub2/disable_os_prober
#. :sl1:
#. Type: boolean
#. Description
#. Very similar to grub2/disable_os_prober
#. :sl1:
#: ../grub-installer.templates:34001 ../grub-installer.templates:35001
msgid "Run os-prober automatically to detect and boot other OSes?"
msgstr ""
"Uruchomić os-prober automatycznie, aby wykryć i uruchomić inne systemy "
"operacyjne?"

#. Type: boolean
#. Description
#. Very similar to grub2/disable_os_prober
#. :sl1:
#. Type: boolean
#. Description
#. Very similar to grub2/disable_os_prober
#. :sl1:
#: ../grub-installer.templates:34001 ../grub-installer.templates:35001
msgid ""
"GRUB can use the os-prober tool to attempt to detect other operating systems "
"on your computer and add them to its list of boot options automatically."
msgstr ""
"GRUB może użyć narzędzia os-prober do próby wykrycia innych systemów "
"operacyjnych na komputerze i automatycznego dodania ich do listy opcji "
"rozruchu."

#. Type: boolean
#. Description
#. Very similar to grub2/disable_os_prober
#. :sl1:
#. Type: boolean
#. Description
#. Very similar to grub2/disable_os_prober
#. :sl1:
#: ../grub-installer.templates:34001 ../grub-installer.templates:35001
msgid ""
"If your computer has multiple operating systems installed, then this is "
"probably what you want. However, if your computer is a host for guest OSes "
"installed via LVM or raw disk devices, running os-prober can cause damage to "
"those guest OSes as it mounts filesystems to look for things."
msgstr ""
"Jeśli na komputerze jest zainstalowanych wiele systemów operacyjnych, "
"prawdopodobnie właśnie tego potrzebujesz. Jeśli jednak komputer jest hostem "
"dla systemów-gości zainstalowanych za pośrednictwem LVM lub surowych "
"urządzeń dyskowych, uruchomienie os-prober może spowodować uszkodzenie tych "
"systemów-gości, ponieważ montuje systemy plików w celu wyszukania rzeczy."

#. Type: boolean
#. Description
#. Very similar to grub2/disable_os_prober
#. :sl1:
#: ../grub-installer.templates:35001
msgid ""
"os-prober did not detect any other operating systems on your computer at "
"this time, but you may still wish to enable it in case you install more in "
"the future."
msgstr ""
"os-prober nie wykrył w tej chwili żadnych innych systemów operacyjnych na "
"tym komputerze, ale nadal możesz chcieć go włączyć w razie instalacji "
"kolejnych w przyszłości."
